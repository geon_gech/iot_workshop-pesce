// Pin Number: DHT11 Data -> NodeMCU D1

#include "DHT.h"	// Install adafruit-DHT sensor library and Adafruit Unified Sensor library.

#define DHTPIN 5  

#define DHTTYPE DHT11   

DHT dht(DHTPIN, DHTTYPE);

float humidity = 0;

void setup() {
  Serial.begin(115200);
  dht.begin();
}

void loop() {
  delay(2000);

  humidity = dht.readHumidity();

  if (isnan(humidity)) {
    Serial.println("Failed to read Humidity data from DHT sensor!");
    return;
  }
  
  Serial.println(humidity);

}


