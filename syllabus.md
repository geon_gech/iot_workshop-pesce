Day 1:

- Introduction to Open Hardware
- Brief explanation of basic concepts
- Introduction to hardware components and software tools
- NodeMCU specifications
- Blink LED
- UART Print Sensor Data
- UART LED Control
- Sensor Interface
- Actuator Interface
- Introduction to Network and IoT

Day 2:

- Wifi Scan
- Wifi Connect
- Wifi Ping
- Wifi Server Send Receive with Python Server
- Send Sensor Data to Server
- Wifi GPIO Control
- Introduction to Thinger.io Cloud Visualization Platform**
- Visualize Sensor Data on Thinger.io**

** To be taken up based on availabitity of time
